#!/bin/bash

set -e

host="$1"

cd /app
composer install --no-scripts

sleep 2s

php artisan -nv migrate
echo "Migrated, Starting server"
cd /app/public
php -S 0.0.0.0:8080